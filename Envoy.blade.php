@servers(['web' => 'laravel-deployer@whitefallen.de'])

@setup
    require __DIR__.'/vendor/autoload.php';
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    try {
        $dotenv->load();
        $dotenv->required(['TWITCH_CLIENT_ID', 'TWITCH_CLIENT_SECRET', 'SPOTIFY_CLIENT_ID', 'SPOTIFY_CLIENT_SECRET'])->notEmpty();
    } catch ( Exception $e )  {
        echo $e->getMessage();
    }
    $repository = 'git@gitlab.com:der-eine-aegyptische-gott/pile-of-shame-backend.git';
    $releases_dir = '/var/www/pile-of-shame/releases';
    $app_dir = '/var/www/pile-of-shame';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
    $twitch_client_id = env('TWITCH_CLIENT_ID');
    $twitch_client_secret = env('TWITCH_CLIENT_SECRET');
    $spotify_client_id = env('SPOTIFY_CLIENT_ID');
    $spotify_client_secret = env('SPOTIFY_CLIENT_SECRET');
@endsetup


@story('deploy')
    clone_repository
    run_composer
    link_storage
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
    touch database/database.sqlite
    chmod -R 775 database
    chgrp -R www-data database
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
    cp .env.example .env
    echo {{$twitch_client_id}} >> .env
    echo {{$twitch_client_secret}} >> .env
    echo {{$spotify_client_id}} >> .env
    echo {{$spotify_client_secret}} >> .env
    php artisan key:generate
    php artisan config:clear
    php artisan config:cache
@endtask

@task('link_storage')
    cd {{ $new_release_dir }}
    php artisan storage:link
@endtask

@task('update_symlinks')
    cd {{ $app_dir }}
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask


