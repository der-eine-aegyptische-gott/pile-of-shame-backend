<?php

use Aerni\Spotify\Facades\SpotifyFacade as Spotify;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/search/{musicName}', function ($musicName) {
    $music = Spotify::searchAlbums($musicName)->get();
    return $music;
});

