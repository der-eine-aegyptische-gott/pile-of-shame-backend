<?php

use Illuminate\Support\Facades\Route;
use MarcReichel\IGDBLaravel\Models\Game;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/search/{gameName}', function ($gameName) {
    $games = Game::search($gameName)->select(['name','summary','first_release_date'])->with(['genres' => ['name'], 'cover' => ['url']])->get();
    return $games;
});
