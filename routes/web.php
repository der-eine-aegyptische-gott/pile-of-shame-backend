<?php

use Illuminate\Support\Facades\Route;
use MarcReichel\IGDBLaravel\Models\Game;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $game = Game::with(['cover' => ['url', 'image_id']])->get();
    return $game;
});
